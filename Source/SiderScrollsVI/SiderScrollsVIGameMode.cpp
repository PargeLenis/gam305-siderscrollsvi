// Copyright Epic Games, Inc. All Rights Reserved.

#include "SiderScrollsVIGameMode.h"
#include "SiderScrollsVICharacter.h"

ASiderScrollsVIGameMode::ASiderScrollsVIGameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = ASiderScrollsVICharacter::StaticClass();	
}
